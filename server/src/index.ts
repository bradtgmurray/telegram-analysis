import 'source-map-support/register'

import * as express from 'express';
import * as path from 'path';

import { MessageDatabase } from './db';

const database_url = process.env.MONGO_URL || 'mongodb://database/telegram';

const db = new MessageDatabase(database_url);
const app = express();

const CLIENT_BUILD_PATH = path.join(__dirname, '../../client/build');

function setupApiRoutes() {
  app.get('/api', (req, res) => {
    res.set('Content-Type', 'application/json');
    const data = {
      message: 'Hello world, Woooooeeeee!!!!'
    };
    res.send(JSON.stringify(data, null, 2));
  });

  app.get('/api/count', async (req, res) => {
    res.set('Content-Type', 'application/json');

    const dateRange: [Date, Date] = await db.getDateRange();
    const data = {
      count: await db.getRecordCount(),
      earliest: dateRange[0],
      latest: dateRange[1]
    };
    res.send(JSON.stringify(data, null, 2));
  });

  app.get('/api/byuser', async (req, res) => {
    res.set('Content-Type', 'application/json');

    let startDate = null;
    let endDate = null;
    if (req.query.startDate && req.query.endDate) {
      startDate = new Date(req.query.startDate);
      endDate = new Date(req.query.endDate);
    }

    const messagesPerUser: any = await db.getMessagesPerUser(startDate, endDate);

    const fetchUsername = async (user) => {
      const userInfo = await db.getUser(user.sender);

      // eslint-disable-next-line no-param-reassign
      user.senderName = `@${userInfo.username}`;
    };
    const promises = messagesPerUser.map(user => fetchUsername(user));
    await Promise.all(promises);

    const data = {
      users: messagesPerUser
    };
    res.send(JSON.stringify(data, null, 2));
  });
}

async function main() {
  if (!await db.connect()) {
    return;
  }

  const PORT = process.env.PORT || 8080;

  setupApiRoutes();

  app.use(express.static(CLIENT_BUILD_PATH));

  // All remaining requests return the React app, so it can handle routing.
  app.get('*', (request, response, next) => {
    if (request.method === 'GET' && request.accepts('html') && !request.is('json') && !request.path.includes('.')) {
      response.sendFile('index.html', { root: CLIENT_BUILD_PATH });
    } else {
      next();
    };
  });

  app.listen(PORT);
  console.log(`Running on http://localhost:${PORT}`);
}

main();
