import { prop, Typegoose, ModelType, InstanceType } from 'typegoose';
import * as mongoose from 'mongoose';

class User extends Typegoose {
  @prop({ required: true, unique: true, index: true })
  id: number;

  username: string;

  @prop({ required: true })
  firstName: string;

  lastName: string;
}

const UserModel = new User().getModelForClass(User);

class Message extends Typegoose {
  @prop({ required: true, unique: true, index: true })
  id: number;

  @prop({ required: true })
  date: Date;

  @prop({ required: true })
  sender: number;

  @prop({ required: true })
  message: string;
}

const MessageModel = new Message().getModelForClass(Message);

export class MessageDatabase {
  mongo_url: string;

  constructor(mongo_url: string) {
    this.mongo_url = mongo_url;

    const db = mongoose.connection;
    db.on('error', console.error.bind(console, 'connection error:'));
    db.once('open', console.error.bind(console, 'DB connection success'));
  }

  async connect(): Promise<boolean> {
    const connectedPromise = new Promise((resolve, reject) => {
      mongoose.connection.once('open', () => {
        console.log('resolved mongoose', typeof mongoose);
        resolve(mongoose);
      });

      mongoose.connection.on('error', (err) => {
        console.error('connection error:', err);
        reject(err);
      });
    });
    
    mongoose.connect(this.mongo_url);
    try {
      await connectedPromise;
      return true;
    } catch (err) {
      return false;
    }
  }

  close(): void {
    mongoose.connection.close();
  }

  async insertMessage(m: Message): Promise<boolean> {
    const result = await MessageModel.findOne({ id: m.id }).exec();
    if (result !== null) {
      // Message already exists
      return false;
    }

    const message = new MessageModel(m);
    await message.save();
    return true;
  }

  async insertSender(u: User): Promise<boolean> {
    const result = await UserModel.findOne({ id: u.id }).exec();
    if (result !== null) {
      // Message already exists
      return false;
    }

    const user = new UserModel(u);
    await user.save();
    return true;
  }

  async getRecordCount(): Promise<number> {
    return MessageModel.countDocuments();
  }

  async getDateRange(): Promise<[Date, Date]> {
    if (await this.getRecordCount() === 0) {
      return Promise.resolve([undefined, undefined]) as Promise<[Date, Date]>;
    }

    const earliestDate = MessageModel.find().sort({ "date": 1 }).limit(1).exec().then(r => r[0].date);
    const latestDate = MessageModel.find().sort({ "date": -1 }).limit(1).exec().then(r => r[0].date);
    return Promise.all([earliestDate, latestDate]);
  }

  async getMessagesPerUser(startDate?: Date, endDate?: Date): Promise<any> {
    let pipeline: any[] = [
      {
        $group: {
          _id: '$sender',
          count: { $sum: 1 },
        },
      },
      {
        $project: {
          sender: '$_id',
          count: '$count',
          _id: 0,
        },
      },
    ];

    if (startDate && endDate) {
      pipeline.unshift({
        $match: {
          date: {
            $gt: startDate,
            $lt: endDate
          }
        }
      });
    }

    return MessageModel.aggregate(pipeline).exec();
  }

  async getUser(userId: number): Promise<User> {
    return await UserModel.findOne({ id: userId }).exec();
  }
}
