import { Client } from 'tdl';

export class TelegramClient {
  client: any;
  phoneNumber: string;
  chatId: string;
  userCache: object;

  constructor(apiId, apiHash, phoneNumber, chatId) {
    this.client = new Client({
      apiId,
      apiHash,
    });

    this.phoneNumber = phoneNumber;
    this.chatId = chatId;

    this.userCache = {};
  }

  async connect() {
    await this.client.connect();
    await this.client.login(() => ({
      phoneNumber: process.env.TELEGRAM_PHONE_NUMBER,
    }));

    // For some reason our saved chat ID doesn't work unless we fetch all chats first
    await this.getAllChats();
  }

  close() {
    this.client.destroy();
  }

  async getAllChats() {
    const chats = await this.client.invoke({
      _: 'getChats',
      offset_order: '9223372036854775807',
      offset_chat_id: 0,
      limit: 100
    }).catch((err) => {
      console.log(err);
    });

    return chats;
  }

  async getChatInfo() {
    const chatRoomInfo = await this.client.invoke({
      _: 'getChat',
      chat_id: this.chatId,
    }).catch((err) => {
      console.log(err);
    });

    return chatRoomInfo;
  }

  async fetchMoreMessages(messageId, limit) {
    const response = await this.client.invoke({
      _: 'getChatHistory',
      chat_id: this.chatId,
      from_message_id: messageId,
      offset: 0,
      limit,
      only_local: false,
    }).catch((err) => {
      console.log(err);
    });
    return response.messages;
  }

  async getUserInfo(userId: number) {
    if (userId in this.userCache) {
      return this.userCache[userId];
    }

    const userInfo = await this.client.invoke({
      _: 'getUser',
      user_id: userId,
    }).catch((err) => {
      console.log(err);
    });

    this.userCache[userId] = userInfo;

    return userInfo;
  }
}
