import 'source-map-support/register'

import { TelegramClient } from './telegram';
import { MessageDatabase } from '../db';

import * as moment from 'moment';
import * as table from 'text-table';

import * as dotenv from "dotenv";
dotenv.config();

var SegfaultHandler = require('segfault-handler');
SegfaultHandler.registerHandler("crash.log"); // With no argument, SegfaultHandler will generate a generic log file name

const database_url = process.env.MONGO_URL || 'mongodb://database/telegram';

async function insertMessage(db, telegramMessage, m): Promise<boolean> {
  // FIXME: Would be great to insert the sender and the message in parallel but I'm bad at js

  const userInfo = await telegramMessage.getUserInfo(m.sender_user_id);
  const dbUser = {
    id: userInfo.id,
    username: userInfo.username,
    firstName: userInfo.first_name,
    lastName: userInfo.last_name,
  }
  await db.insertSender(dbUser);

  const dbMessage = {
    id: m.id,
    date: (m.date * 1000), // convert from unix timetamp to js (millis)
    sender: parseInt(m.sender_user_id, 10),
    message: undefined
  };

  if (m.content._ === 'messageText') {
    dbMessage.message = m.content.text.text;
  } else {
    dbMessage.message = `<${m.content._}>`;
  }

  console.log(`[${dbMessage.id}] [${moment.unix(dbMessage.date / 1000)}] [${dbMessage.sender}] ${dbMessage.message}`);
  return await db.insertMessage(dbMessage);
}

async function updateMessagesInDatabase(db, telegramClient) {
  const EXIT_ON_FIRST_DUPLICATE = true;

  const chatInfo = await telegramClient.getChatInfo();
  let messageId = chatInfo.last_message.id;

  for (let iteration = 0; iteration < 10; iteration += 1) {
    console.log('fetching messages');

    // eslint-disable-next-line no-await-in-loop
    const messages = await telegramClient.fetchMoreMessages(messageId, 100);

    let allMessagesWereNew = true;

    // Can't use forEach or some here due to await
    for (let i = 0; i < messages.length; i += 1) {
      const m = messages[i];

      // eslint-disable-next-line no-await-in-loop
      const wasMessageNew = await insertMessage(db, telegramClient, m);

      if (EXIT_ON_FIRST_DUPLICATE && !wasMessageNew) {
        console.log(`${m.id} already exists in our database!`);
        allMessagesWereNew = false;
        break;
      }
    }

    if (!allMessagesWereNew) {
      break;
    }

    // Advance to the final messageId and continue fetching messages
    messageId = messages[messages.length - 1].id;
  }
}

async function main(): Promise<void> {
  const db = new MessageDatabase(database_url);
  if (!await db.connect()) {
    return;
  }

  const telegramClient = new TelegramClient(
    process.env.TELEGRAM_API_ID,
    process.env.TELEGRAM_API_HASH,
    process.env.TELEGRAM_PHONE_NUMBER,
    process.env.TELEGRAM_CHAT_ID,
  );

  await telegramClient.connect();

  await updateMessagesInDatabase(db, telegramClient);

  telegramClient.close();
  db.close();
}

main();
