set -e # exit on errors

docker build . -t tdlib-build

cidfile=$(mktemp)
rm $cidfile

docker run --cidfile $cidfile tdlib-build

docker cp $(cat $cidfile):/root/td/build/libtdjson.so .

docker rm tdlib-build
