import React, { Component } from 'react';
import { Button } from "@blueprintjs/core";
import { DateRangePicker } from "@blueprintjs/datetime";

import * as moment from 'moment'

class ControlPanel extends Component {
  constructor(props) {
    super(props);

    this.state = {
      dateSelectorShowing: false
    };

    this.selectedStartDate = null;
    this.selectedEndDate = null;
  }

  dateSelected = (dates) => {
    [this.selectedStartDate, this.selectedEndDate] = dates;
  }

  selectorButtonClicked = () => {
    if (this.state.dateSelectorShowing) {
      this.props.dateRangeSelected(this.selectedStartDate, this.selectedEndDate);
    }

    this.setState(state => ({ dateSelectorShowing: !state.dateSelectorShowing }));
  }

  undoButtonClicked = () => {
    this.selectedStartDate = null;
    this.selectedEndDate = null;

    this.props.dateRangeSelected(null, null);
  }

  render() {
    const { dateSelectorShowing } = this.state;

    const renderDate = d => moment(d).format('MMM Do YYYY, h:mm:ss a')
    let buttonText;
    if (dateSelectorShowing) {
      buttonText = 'Done';
    } else {
      buttonText = `${renderDate(this.props.startDate) } - ${ renderDate(this.props.endDate) }`;
    }


    return (
      <div className="control-panel">
        <div className="control-panel-left">
          Total Message Count: { this.props.messageCount }
        </div>
        <div className="control-panel-right">
          <div className="control-panel-date-picker">
            { this.selectedStartDate && <Button icon='undo' onClick={ this.undoButtonClicked } /> }
            <Button onClick={ this.selectorButtonClicked }>{ buttonText }</Button>
            { dateSelectorShowing &&
              <div className="control-panel-date-picker-popup">
                <DateRangePicker onChange={ this.dateSelected }/>
              </div>
            }
          </div>
        </div>
      </div>
    );
  }
}

export default ControlPanel;