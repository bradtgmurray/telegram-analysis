import React, { Component } from 'react';
import { Alignment, Navbar } from '@blueprintjs/core';

class Header extends Component {
  render() {
    return (
      <Navbar className='bp3-dark'>
        <Navbar.Group align={Alignment.LEFT}>
          <Navbar.Heading>Telegram Analysis</Navbar.Heading>
        </Navbar.Group>
      </Navbar>
    );
  }
}

export default Header;