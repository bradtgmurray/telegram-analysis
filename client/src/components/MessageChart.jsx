import React, { Component } from 'react';
import {BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip} from 'recharts';

class CustomizedAxisTick extends Component {
  render() {
    const {x, y, payload} = this.props;
		
   	return (
    	<g transform={`translate(${x},${y})`}>
        <text x={4} y={0} dy={16} textAnchor="end" fill="#666" transform="rotate(-40)">{payload.value}</text>
      </g>
    );
  }
};

class MessageChart extends Component {
  render() {
    const users = this.props.children;

    users.sort((a, b) => b.count - a.count);

    return (
      <BarChart width={this.props.width} height={600} data={users} margin={{top: 25, right: 30, left: 20, bottom: 75}}>
        <CartesianGrid strokeDasharray="3 3"/>
        <XAxis dataKey="senderName" tick={<CustomizedAxisTick/>} minTickGap={ -100 } />
        <YAxis label={{ value: '# Messages', angle: -90, position: 'insideLeft' }} />
        <Tooltip/>
        <Bar dataKey="count" fill="#0E5A8A" />
      </BarChart>
    );
  }
};

export default MessageChart;