import React, { Component } from 'react';
import ContainerDimensions from 'react-container-dimensions';

import { NonIdealState } from "@blueprintjs/core";

import './App.css';
import Header from './components/Header';
import MessageChart from './components/MessageChart';
import ControlPanel from './components/ControlPanel';

import 'normalize.css/normalize.css';
import '@blueprintjs/core/lib/css/blueprint.css';
import '@blueprintjs/datetime/lib/css/blueprint-datetime.css';
import '@blueprintjs/icons/lib/css/blueprint-icons.css';

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      messageCount: null,
      startDate: null,
      endDate: null,
      users: null,
    }
  }

  componentDidMount() {
    this.fetchFullDateRange();
  }

  fetchFullDateRange() {
    fetch('/api/count')
      .then(response => response.json())
      .then((data) => {
        this.fetchMessages(new Date(data.earliest), new Date(data.latest));
      });
  }

  fetchMessages(startDate, endDate) {
    let url = new URL(window.location.href + 'api/byuser');
    if (startDate && endDate) {
      url.searchParams.append('startDate', startDate.toISOString());
      url.searchParams.append('endDate', endDate.toISOString());
    }

    fetch(url)
      .then(response => response.json())
      .then((data) => {
        const messageCount = data.users.reduce((sum, user) => sum + user.count , 0);

        this.setState({
          messageCount,
          startDate,
          endDate,
          users: data.users
        });
      });
  }
  
  isLoading() {
    return this.state.messageCount === null;
  }

  dateRangeSelected = (startDate, endDate) => {
    console.log(`Date selected - ${startDate} - ${endDate}`);
    if (!startDate || !endDate) {
      this.fetchFullDateRange();
    } else {
      this.fetchMessages(startDate, endDate);
    }
  }

  render() {
    let body;
    if (this.isLoading()) {
      body = (
        <NonIdealState title="Loading..."/>
      )
    } else {
      body = (
        <div>
          <ControlPanel
              messageCount={ this.state.messageCount }
              startDate={ this.state.startDate }
              endDate={ this.state.endDate }
              dateRangeSelected={ this.dateRangeSelected }/>
          <ContainerDimensions>
            <MessageChart>{ this.state.users }</MessageChart>
          </ContainerDimensions>
        </div>
      )
    }

    return (
      <div className="App">
        <Header />
        { body }
      </div>
    );
  }
}

export default App;
